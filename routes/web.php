<?php


use App\Notifications\ApplicationApproved;
use App\User;
use App\Student;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => 'company'], function(){
    Route::get('/company/category/add', 'CompanyController@addCategory');
    Route::post('/company/category/postAdd','CompanyController@postAddCategory');
    Route::get('/company/add-job-advert',   'CompanyController@addJobAdvert');
    Route::post('/company/post-job-advert', 'CompanyController@postAddJobAdvert');
    Route::get('/company/view-job-adverts', 'CompanyController@viewJobAdverts');
    Route::get('/company/{jpaid}/delete-job-advert', 'CompanyController@deleteJobAdvert');
    Route::get('/company/view-applications',  'CompanyController@viewApplications');
    Route::get('/company/approve-application/{apid}', 'CompanyController@approveApplication');
    Route::get('/company/decline-application/{apid}', 'CompanyController@declineApplication');
    Route::get('/company/delete/{apid}/application', 'CompanyController@deleteApplication');
    Route::get('company/dashboard','companyController@dashboard');
});

Route::get('company/send','CompanyController@send');
Route::get('/send-approval-notification',  function () {
    
    Student::find(2)->notify((new ApplicationApproved));
    return view('welcome');

});

Route::get('/sideber', 'HomeController@getSideber');


Route::group(['middleware' => 'student'], function(){
    Route::get('student/dashboard','StudentController@dashboard');
    Route::get('student/apply', 'StudentController@getStartApplication');
    Route::post('student/post-application', 'StudentController@postApplication');
    Route::get('studentId/{stid}', 'StudentController@getStudentId');
});

// Authentication for company
Route::get('company/register', 'CompanyController@registerCompany');
Route::post('post-register-company', 'CompanyController@postRegisterCompany');
Route::get('company/login','AuthenticationController@logincompany');
Route::post('company/login','AuthenticationController@postLogincompany');

//Authentication for Students
Route::get('student/register', 'StudentController@registerStudent');
Route::post('post-register-student', 'StudentController@postRegisterStudent');
Route::get('student/login','AuthenticationController@loginStudent');
Route::post('student/login','AuthenticationController@postLoginStudent');

// GENERAL
Route::get('forgot-password','AuthenticationController@forgotPassword');
Route::get('change-password','AuthenticationController@changePassword');
Route::get('reset-password','AuthenticationController@resetPassword');

Route::post('forgot-password','AuthenticationController@postForgotPassword');
Route::post('change-password','AuthenticationController@postChangePassword');
Route::post('reset-password','AuthenticationController@postResetPassword');



Route::get('/student/password/reset', 'AuthenticationController@studentForgotPassword');
Route::get('/company/password/reset', 'AuthenticationController@companyForgotPassword');


Route::post('post-student-forget-password', 'AuthenticationController@poststudentForgotPassword');
Route::post('post-company-forget-password', 'AuthenticationController@postcompanyForgotPassword');

Route::get('logout','HomeController@logout');
Route::get('logout-user/{user}','AuthenticationController@logoutUser');


