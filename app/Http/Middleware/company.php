<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class company
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // if(Auth::user()->role == 'company' || Auth::user()->role == 'admin')
        //     return $next($request);
        // else{
        //     $request->session()->flash("error","You are not authorized to access that page.");
        //     return redirect("/");
        // }

        if($request->session()->has('company'))
	        return $next($request);
    	else
        	return redirect('company/login')->with("error","You are not permitted to access that route!");
    }
}
