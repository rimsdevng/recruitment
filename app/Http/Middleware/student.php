<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class student
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // if(Auth::user()->role == 'student' || Auth::user()->role == 'admin')
        //     return $next($request);
        // else{
        //     $request->session()->flash("error","You are not authorized to access that page.");
        //     return redirect("/");
        // }

        
    	if($request->session()->has('student'))
        return $next($request);
        else
        return redirect('student/login')->with("error", "Login to Proceed!");
    
    }
}
