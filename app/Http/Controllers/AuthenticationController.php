<?php

namespace App\Http\Controllers;

use App\confirmation;
use App\Mail\forgotPasswordMail;
use App\Mail\SendForgetPasswordEmail;

use App\Company;
use App\Student;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class AuthenticationController extends Controller
{


	public function __construct() {}

	public function resetPassword(Request $request) {

		$token = $request->input('token');
		$email = $request->input('email');
		$role = $request->input('role');

		if(confirmation::where('email',$email)->where('token',$token)->where('role',$role)->count() <= 0){

			session()->flash('error','Invalid Token. Please attempt to reset your password again.');
			return redirect('/');
		}

		$confirmation = confirmation::where('email',$email)->where('token',$token)->where('role',$role)->first();

		$confirmation->delete();

		return view('resetPassword',[
			'change' => true,
			'role' => $role,
			'email' => $email
		]);

	}

	public function postResetPassword(Request $request) {

		$role = $request->input('role');
		$email = $request->input('email');
		$newPassword = $request->input('newPassword');

		if($role == 'company'){
			$company = Company::where('email',$email)->first();
			$company->password = bcrypt($newPassword);
			$company->save();
			session()->flash('success','Password Reset.');
			return redirect('company/login');

		}
		// Note: initially the email in the where clause was receiving a phone number
		if($role == 'student'){
			$student = Student::where('email',$email)->first();
			$student->password = bcrypt($newPassword);
			$student->save();
			session()->flash('success','Password Reset.');
			return redirect('student/login');

		}

		if($role == 'admin'){
			$admin = User::where('email',$email)->first();
			$admin->password = bcrypt($newPassword);
			$admin->save();
			session()->flash('success','Password Reset.');
			return redirect('login');
		}


		return redirect('/');

	}


	//student Authentication
    public function loginstudent() {
        if(session()->has('student')) return redirect('/');

        return view('dashboard.student.login');
    }

    public function postLoginstudent( Request $request ) {
		$username = $request->email;
		$password = $request->password;
		$student = student::where('email',$username)->get();

		if(count($student) <=  0){ // check that the username exists

			session()->flash('error',"Account doesn't exist");
			return redirect()->back();

		}

		$student = student::where('email',$username)->first();

		// if(!isset($student->Class)){
		// 	session()->flash('error','You are not assigned to any class yet. Please contact IT');
		// 	return redirect()->back();
		// }

		if (password_verify($password,$student->password)){
			$student->role = 'student';
			// $student->Subjects;
			// $student->class = $student->Class->name;

			session()->put('student',$student);
			return redirect('student/dashboard');
			// return redirect('/student/apply');
		}else{
			session()->flash('error','Password is incorrect');
			return redirect()->back();
		}


    }

	//company Authentication
	public function logincompany() {
		if(session()->has('company')) return redirect('company/dashboard');

		return view('dashboard.company.login');
	}

	public function postLogincompany( Request $request ) {
		$username = $request->email;
		$password = $request->password;
		$company = company::where('email',$username)->get();

		if(count($company) <=  0){ // check that the username exists

			session()->flash('error',"Account doesn't exist");
			return redirect()->back();

		}

		$company = company::where('email',$username)->first();

		// if(!isset($company->Class)){
		// 	session()->flash('error','You are not assigned to any class yet. Please contact IT');
		// 	return redirect()->back();
		// }

		if (password_verify($password,$company->password)){
			$company->role = 'company';
			// $company->Subjects;
			// $company->class = $company->Class->name;

			session()->put('company',$company);
			return redirect('company/dashboard');
			// return redirect('/company/apply');
		}else{
			session()->flash('error','Password is incorrect');
			return redirect()->back();
		}

	}


	public function logOutUser(Request $request,$user ) {
		 session()->remove($user);
		 if ($user == 'company'){
			 return redirect('company/login');

		 }elseif ($user == 'student'){
			 return redirect('student/login');
		 }else{
			return redirect('/login');
		 }
	}


	public function changePassword() {

		if(auth()->check()) return view('changePassword');
		if(session()->has('student')) return view('dashboard.student.changePassword');
		if(session()->has('company')) return view('dashboard.company.changePassword');
		return redirect('/');
	}


	public function postChangePassword( Request $request ) {
		$currentPassword = $request->input('currentPassword');
		$newPassword = $request->input('newPassword');
		$confirmPassword = $request->input('confirmPassword');

		if($newPassword != $confirmPassword){
			session()->flash('error','Confirmed password does not match your new password. Please try again.');
			return redirect()->back();
		}

		if(auth()->check())
		{
			$user = User::find(auth()->user()->uid);

			if(!password_verify($currentPassword, $user->password)) {
				session()->flash('error','Current Password is wrong');
				return redirect()->back();
			}

			$user->password = bcrypt($newPassword);
			$user->save();
			session()->flash('success','Password Changed.');
			return redirect()->back();
		}


		if(session()->has('student')){
			$stid = session()->get('student')->stid;
			$student = Student::find($stid);

			if(!password_verify($currentPassword, $student->password)) {
				session()->flash('error','Current Password is wrong');
				return redirect()->back();
			}

			$student->password = bcrypt($newPassword);
			$student->save();
			session()->flash('success','Password Changed.');
			return redirect()->back();
		}

		if(session()->has('company')){
			$empno = session()->get('company')->empno;
			$company = Company::find($empno);

			if(!password_verify($currentPassword, $company->password)) {
				session()->flash('error','Current Password is wrong');
				return redirect()->back();
			}

			$company->password = bcrypt($newPassword);
			$company->save();
			session()->flash('success','Password Changed.');
			return redirect()->back();
		}



		return redirect()->back();
	}

	public function companyForgotPassword() {
		return view('dashboard.company.forgetPassword');
	}

	public function postcompanyForgotPassword(Request $request) {
		$email = $request->input('email');
		$token = Str::random(24);

		$confirmation = new confirmation();
		$confirmation->email = $email;
		$confirmation->token = $token;
		$confirmation->role = "company";
		$confirmation->save();


		Mail::to($email)->send(new SendForgetPasswordEmail($token, $email,"company"));
		session()->flash('success','Check your mail for a password reset link.');
		return redirect()->back();
	}

	public function studentForgotPassword() {
		return view('dashboard.student.forgetPassword');
	}

	public function poststudentForgotPassword(Request $request) {
		$email = $request->input('email');
		$token = strtoupper(self::random_number(6));


		$confirmation = new confirmation();
		$confirmation->email = $email;
		$confirmation->token = $token;
		$confirmation->role = "student";
		$confirmation->save();

		Mail::to($email)->send(new SendForgetPasswordEmail($token, $email,"student"));
		session()->flash('success','Check your mail for a password reset link.');
		return redirect()->back();

		// $message = "Your password reset code is $token";
		// $this->sendSms($phone,$message);
		// session()->flash('success','Check your phone for a password reset code.');
		// session()->flash('showReset',"true");
		// session()->flash('resetPhone',$phone);
	}

	public function adminForgotPassword() {
		return view('forgotPassword');
	}

	public function postAdminForgotPassword(Request $request) {
		$email = $request->input('email');
		$token = Str::random(24);

		$confirmation = new confirmation();
		$confirmation->email = $email;
		$confirmation->token = $token;
		$confirmation->role = "admin";
		$confirmation->save();


		Mail::to($email)->send(new forgotPasswordMail($token, $email,"admin"));
		session()->flash('success','Check your mail for a password reset link.');
		return redirect()->back();
	}


	static  function random_number($length, $keyspace = '0123456789')
	{
		$str = '';
		$max = mb_strlen($keyspace, '8bit') - 1;
		for ($i = 0; $i < $length; ++$i) {
			$str .= $keyspace[random_int(0, $max)];
		}
		return $str;
	}


	function sendSms($phone,$message) {
		$message = urlencode( $message );
		file_get_contents( "https://www.bulksmsnigeria.com/api/v1/sms/create?dnd=2&api_token=rVY7mjk9AfG2CCx9KdzHkqB1CSVCoyOvNxEvKLdnhEVbtrtcZ7uM8ElPeC7S&from=HENDON&to=$phone&body=$message" );
	}




}
