<?php

namespace App\Http\Controllers;

use App\application;
use App\Student;
use App\Company;
use App\Notification;
use App\User;
use App\Jobcategory;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use http\Exception;
use Illuminate\Support\Facades\App;
use PHPExcel_IOFactory;
use PHPExcel_Shared_Date;



class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    

}
