<?php

namespace App\Http\Controllers;

use App\application;
use App\Student;
use App\Jobcategory;
use App\JobAdvert;
use App\JobAdvertApplication;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Faker\Provider\Company;
use http\Exception;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\App;
use PHPExcel_IOFactory;
use PHPExcel_Shared_Date;

class StudentController extends Controller
{
    //


	public function registerStudent(){
		return view ('dashboard.student.register');
	}

	public function postRegisterStudent(Request $request){

		$this->validate($request, [
			'email' => 'email',
			'password' => 'required|confirmed|min:8',
		]);

		// $studentno = 'R10O1'. Str::random(5);
        $student = new Student();		
		$student->sname = $request->input('sname');
		$student->fname = $request->input('fname');
		$student->email = $request->input('email');
		$student->username = $request->input('username');
		$student->gender = $request->input('gender');
		$student->phone = $request->input('phone');
		$student->address = $request->input('address');
		// $student->password = bcrypt("12345678");    
		$student->password = bcrypt($request->input('password'));
		// $student->studentno = $studentno;
		$student->studentno = 'SID-'.$request->input('studentno');
		$status = $student->save();
		
		if ($status){
			return redirect('/student/login')->with('success','Student Created Sucessfully.');
		}else{
			session()->flash('error','Something went Wrong, Try again');
			return redirect()->back();
		}

	}
	public function dashboard(){
		return view ('dashboard.student.dashboard');
	}

    public function getStartApplication(){
		$student = Student::where('stid', session()->get('student')->stid);
		$jobadverts = JobAdvert::all();
		$categories = Jobcategory::all();
        return view('dashboard.student.apply',[
				'student' => $student,
				'categories' => $categories,
				'jobadverts'  => $jobadverts
		]);
    }

	
	
    public function postApplication(Request $request){

		try{

		DB::beginTransaction();

		$sid = session()->get('student')->stid;	
		$student = Student::find($sid);
		$student->status='Applied';
		$student->save();

		 if($request->hasFile('document')){
			$inputFileName = $request->file('document')->getClientOriginalName();
			$request->file('document')->move('uploads/documents/',$inputFileName);
			$url = url('uploads/documents/' . $inputFileName);			
		} else {
		$url = url('img/default-image.png');
		}

        $applicationid = 'A19-'. Str::random(5);
        $application = new Application();
		$application->sname = $student->sname;
		$application->fname = $student->fname;
		$application->gender = $student->gender;
		$application->address = $student->address;
		$application->email = strtolower($student->email);
		$application->phone = $student->phone;	
		$application->status = 'Awaiting';
		$application->jpaid = $request->input('jpaid'); 
		$application->jcid = $request->input('jcid');
		$application->stid = $student->stid;
		$application->studentNo = $student->studentno;
		$application->applicationNo = $applicationid;
		$application->filecv = $url;
		$application->comment = strtolower($request->input('comment'));
		$status = $application->save();

		$jobadvertApplication 			= new JobAdvertApplication();
		$jobadvertApplication->jpaid 	= $application->jpaid;
		$jobadvertApplication->apid 	= $application->apid;
		$jobadvertApplication->save();

		DB::commit();
            return redirect('/student/dashboard')->with('success','Application Submitted successfully!');
        }
        catch(\Exception $exception){
            return $exception->getMessage();
            session()->flash('error',"Something went wrong. Please try again or contact Technical Staff.");
            
			return redirect()->back();
        }

	}
	

	public function getStudentId($stid){

        $studentId = Student::findOrFail($stid);

        return response()->json($studentId, 200);
	}
}
