<?php

namespace App\Http\Controllers;

use App\Application;
use App\Student;
use App\Company;
use App\Notification;
use App\Jobcategory;
use App\JobAdvert;
use App\User;
use App\Events\SendApprovedApplication;
use App\Events\NewAppointmentApprovedEvent;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendAppointmentApprovalEmail; 
use Illuminate\Support\Str;
use http\Exception;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\App;
use PHPExcel_IOFactory;
use PHPExcel_Shared_Date;


class CompanyController extends Controller
{
    public function dashboard(){
        $approvedApplications = Application::where('status','Approved')->get();
        $pendingApplications = Application::where('status','Awaiting')->get();
        $advertsPublished= JobAdvert::all();

		return view ('dashboard.company.dashboard', [
            'approvedApplications' => $approvedApplications, 
            'pendingApplications' => $pendingApplications,
            'advertsPublished' => $advertsPublished

        ]);
    }

 
    public function registerCompany(){
        return view ('company_register');
    }

    public function postRegisterCompany (Request $request) {
       
        $this->validate($request, [
			'email' => 'email',
			'password' => 'required|confirmed|min:8',
        ]);

        try{

        DB::beginTransaction();
        
        if($request->hasFile('image')){
            $destinatonPath = '';
            $filename = '';
    
            $file = Input::file('image');
            $destinationPath = public_path().'staff/images/';
            $filename = str_random(6).'_'.$file->getClientOriginalName();
            $uploadSuccess = $file->move($destinationPath, $filename);
        }else {
            $filename = url('/images/default-image.png');

        }
        
        
        $companyno = 'CNO-'. Str::random(5);

		$company = new Company();
		$company->companyid = $companyno;
		$company->companyName = $request->input('companyName');
		$company->companyWebsite = $request->input('companyWebsite');
		$company->companyAddress  = $request->input('companyAddress');
		$company->phone = $request->input('phone');
		$company->email = strtolower($request->input('email'));
        // $company->password = bcrypt("12345678");    
        $company->password = bcrypt($request->input('password'));
        // image to be added 
        $company->save();

        DB::commit();            
            return redirect('/company/login')->with('success','Company Created Successfully');
            
        }catch (\Exception $exception){


            return $exception->getMessage();
            session()->flash('error',"Something went wrong. Please try again or contact IT.");

            return redirect()->back();
        }



    }
  
    public function addCategory(){
        return view ('jobs.categories.add');
    }

    public function postAddCategory(Request $request){
        
        $categories = new Jobcategory();
        $categories->name = $request->input('name');
        $categories->description = $request->input('description');		
        $categories->save();
            return redirect()->back()->with('success', 'Job Category Added');
    }
    
    public function addJobAdvert(){
        $companies = Company::all();
        $categories = Jobcategory::all();
        return view ('dashboard.company.jobAdvert',[
            'companies' => $companies,
            'categories' => $categories 
        ]);
    }
    public function postAddJobAdvert(Request $request){

        $jobRefNo = 'JRN-'. Str::random(5);
        $job = new JobAdvert();
		$job->jobRefNo = $jobRefNo;
        $job->jcid = $request->input('jcid');
        $job->empno = $request->input('empno');
		$job->positionTitle = $request->input('positionTitle');
		$job->positionDescription = $request->input('positionDescription');
		$job->positionDate  = $request->input('positionDate');
		$job->closingDate = $request->input('closingDate');
        $job->closingTime = $request->input('closingTime');
        $job->jobDetails = $request->input('jobDetails');
        $job->jobInstruction = $request->input('jobInstruction');
        $job->status = 'isAvailable';    
        $job->save();
            return redirect()->back()->with('success','Job Advert Created Successfully');
       
    }

    public function viewJobAdverts(){
        $companies = Company::all();
        $categories = Jobcategory::all();
        $jobs = JobAdvert::orderBy('created_at','desc')->paginate(10);
        return view('dashboard.company.viewAdverts', [
            'categories' => $categories,
            'jobs' => $jobs,
            'companies' => $companies
        ]);
    }


    public function deleteJobAdvert($jpaid){
        $jobadvert = JobAdvert::destroy($jpaid);
        if ($jobadvert){
            session()->flash('success','Job Advert Record Deleted successfully');
        }else{
            session()->flash('error','Sorry something went wrong');
        }
        return redirect()->back();
    }

    public function viewApplications(){
        $applications = Application::orderBy('created_at','desc')->paginate(5);
        return view ('dashboard.company.viewApplications',[
            'applications' => $applications
        ]);
    }
    
    public function approveApplication(Request $request, $apid){
        $application = Application::findorfail($apid);
        $application->status = 'Approved';
        $status = $application->save();

         if($status){
            event(new NewAppointmentApprovedEvent($application));   
             return redirect()->back()->with('success', 'Application Approved');
         }else{
			session()->flash('error','Something went Wrong, Try again');
			return redirect()->back();
        }
    }

    public function declineApplication(Request $request, $apid){
        $application = Application::findorfail($apid);
        $application->status = 'UnQualified';
        $status = $application->save();

         if($status){
            event(new NewAppointmentApprovedEvent($application));   
             return redirect()->back()->with('success', 'Application Declined Successfully');
         }else{
			session()->flash('error','Something went Wrong, Try again');
			return redirect()->back();
        }
    }

    public function deleteApplication($apid){
        $application = Application::destroy($apid);
        if ($application){
            session()->flash('success','Application Record Deleted successfully');
        }else{
            session()->flash('error','Sorry something went wrong');
        }   
        return redirect()->back();
    }

    
    
    
    
    
    
    public function send($application){
        $title = $application->applicationNo;
        $studentno = $application->studentno;
   
        // you can parse in the email directly for fast response
        $email = $application->email;
        Mail::to($email)->send(new SendMailable($application));    
       
        return response()->json(['message' => 'Request completed']);
        
    }

    public function sendNotification() {
		return view('admin.sendNotification');
    }

    //push	persist and redirect  
	public function postSendNotification( Request $request ) {
		$message = $request->input('message');

		$this->sendMessage($message);

		$notification = new Notification();
		$notification->message = $message;
		$notification->apid = Auth::user()->uid;
		$notification->save();
		$request->session()->flash('success','Message sent to all users.');

		return redirect('send-notification');
	}

	public function sentNotifications() {
		$notifications = Notification::all()->reverse();
		return view('admin.notifications',[
			'notifications' => $notifications
		]);
	}

    function sendMessage($message){}

    
}
