<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


class Registration1Controller extends Controller
{

    public function create()
    {
        return view('company_register');
    }

    public function store()
    {
        $this->validate(request(), [
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required'
        ]);

        $user = User::create(request(['name', 'email', 'password']));

        auth()->login($user);

        return redirect()->to('/company-login');
    }
}
