<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobAdvertApplication extends Model
{
    //
    protected $primaryKey = 'jaid';
	protected $table = 'jobdvert_application';
	protected $guarded = [];
}
