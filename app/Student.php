<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    //
    use Notifiable;

    protected $table = 'students';
    

    protected  $primaryKey = 'stid';
    protected  $guarded = [ ];
}
