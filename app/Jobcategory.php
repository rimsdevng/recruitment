<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jobcategory extends Model
{
    //
    
    protected $table = 'jobcategories';
    protected  $primaryKey = 'jcid';
    protected  $guarded = [ ];

    public function JobAdvert() {
        // return $this->hasMany(application::class,'prid','prid');         -> return an array and not an object
        
        //   return $this->belongsTo('App\JobAdvert', 'jpaid');
          return $this->hasMany(JobAdvert::class,'jcid','jcid');
        }

      
}
