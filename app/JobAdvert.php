<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobAdvert extends Model
{
    //
    
    
    protected $table = 'job_position_adverts';
    protected  $primaryKey = 'jpaid';
    protected  $guarded = [ ];

    public function Jobcategory() {
		return $this->belongsTo(Jobcategory::class,'jcid', 'jcid');
    }

    public function Company() {
		return $this->belongsTo(Company::class,'empno', 'empno');
    }


    public function Application() {
      return $this->belongsToMany(Application::class,'jobdvert_application','jpaid','apid');
  }

}
