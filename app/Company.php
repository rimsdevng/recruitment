<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    //
    protected $table = 'companies';
    protected  $primaryKey = 'empno';
    protected  $guarded = [];
    
    // public function JobAdvert() {
	// 	return $this->belongsTo(JobAdvert::class,'empno', 'empno');
    // }

    
    public function JobAdvert() {
        // return $this->hasMany(application::class,'prid','prid');         -> return an array and not an object
        return $this->belongsTo('App\jobAdvert', 'empno');
    }
    

}
