<?php

namespace App\Listeners;

use Illuminate\Support\Facades\Mail;
use App\Mail\SendAppointmentApprovalEmail; 
use App\Events\NewAppointmentApprovedEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendAppointmentApprovedListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NewAppointmentApprovedEvent  $event
     * @return void
     */
    
    public function handle(NewAppointmentApprovedEvent $event)
    {
        Mail::to($event->application->email)->send(new SendAppointmentApprovalEmail($event->application));
    }
}
