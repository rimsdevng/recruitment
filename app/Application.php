<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Application extends Model
{
    //
    protected $table = 'applications';
    protected  $primaryKey = 'apid';
    protected  $guarded = [];
    

    public function Jobadvert() {
        return $this->belongsTo(JobAdvert::class,'jpaid','jpaid');
    }
}
