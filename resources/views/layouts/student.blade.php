<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Recruitment:Student</title>
    <link href="{{ url('/images/logo.png') }}" rel="icon" type="image/png">

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.15/angular.min.js"></script>
    <script>
        var myApp = angular.module("myapp", []);
        myApp.controller("PasswordController", function($scope) {
            var strongRegex = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})");
            var mediumRegex = new RegExp("^(((?=.*[a-z])(?=.*[A-Z]))|((?=.*[a-z])(?=.*[0-9]))|((?=.*[A-Z])(?=.*[0-9])))(?=.{6,})");
            $scope.passwordStrength = {
                "float": "left",
                "width": "100px",
                "height": "25px",
                "margin-left": "5px"
            };
            $scope.analyze = function(value) {
                if(strongRegex.test(value)) {
                    $scope.passwordStrength["background-color"] = "blue";
                } else if(mediumRegex.test(value)) {
                    $scope.passwordStrength["background-color"] = "green";
                } else {
                    $scope.passwordStrength["background-color"] = "red";
                }
            };
        });
    </script>
</head>
<body ng-app="myapp">
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                   <img src="{{ url('images/logo.png') }}" alt="Recruitment">
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">

                        <li class="nav-item">
                            <a class="nav-link" href="{{ url('/student/dashboard') }}">Home</a>
                        </li> 
                           
                        <!-- Authentication Links -->
                        {{--  @guest  --}}
                        @if(session()->has('student'))

                        
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{--  {{ Auth::user()->name }}  --}}
                                    <span class="caret">
                                        <i class="fa fa-user"></i>{{session()->get('student')->fname}} <span> {{session()->get('student')->sname}} </span>
                                    </span>
                                    
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    @php(  $user = session()->get('student'))
                                        <a class="dropdown-item"  href="{{url('logout-user/'.$user->role)}}" align="center" class="btn btn-success">Logout</a>
                                </div>
                            </li>
                        @else
                        
                        
                        @endif
                        {{--  @endguest  --}}
                    </ul>
                </div>
            </div>
        </nav>
    <div ng-controller="PasswordController">
        <main class="py-4">
            @yield('content')
        </main>
    </div>
    </div>

    
    {{--  <script src="{{ url('js/bootstrap.js') }}"></script>  --}}
    <script src="{{ url('js/jquery-2.2.3.min.js') }}"></script>
    <script>
       
        //get studentid start
        $(document).ready(function ($) {
    
            var stid = {{ session()->get('student')->stid  }}
    
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
    
            $.ajax({
                url: `{{ URL::to('/studentId') }}/${stid}`,
                dataType: 'json',
                method: 'GET',
                success: function (data) {
                    console.log(data);
    
                    $('#branchid').val(data.stid);
                },
                error :  function(error) {
                    console.log(error)
                }
            })
    
        });
    
        //get branch end
    </script>
</body>
</html>
