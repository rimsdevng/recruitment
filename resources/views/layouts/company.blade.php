<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Recruitment:Company</title>
    <link href="{{ url('/images/logo.png') }}" rel="icon" type="image/png">

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    <img src="{{ url('images/logo.png') }}" alt="Recruitment">

                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">
                        
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        {{--  @guest  --}}
                        <li class="nav-item">
                            <a class="nav-link" href="{{ url('/company/dashboard') }}">Home</a>
                        </li>  
                        @if(session()->has('company'))

                            @if(url()->current() != url('/company/dashboard')) 
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ url('/company/add-job-advert') }}">Create Advert</a>
                                </li>  
                            
                                <li class="nav-item" style="padding-right:20px;">
                                    <a class="nav-link" href="{{ url('/company/view-job-adverts') }}">View Jobs Adverts</a>
                                </li>

                                <li class="nav-item" style="padding-right:20px;">
                                    <a class="nav-link" href="{{ url('/company/view-applications') }}">View Applicants</a>
                                </li>
                                
                            @endif
                        
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    <span class="caret">
                                      <strong>  {{session()->get('company')->companyName}} </strong>
                                    </span>
                                    
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    @php(  $user = session()->get('company'))
                                        <a class="dropdown-item"  href="{{url('logout-user/'.$user->role)}}" align="center" class="btn btn-success">Logout</a>
                                </div>
                            </li>
                        @else
                        
                        
                        @endif
                        {{--  @endguest  --}}
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            @yield('content')
        </main>
    </div>
</body>
</html>
