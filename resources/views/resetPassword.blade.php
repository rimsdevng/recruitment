@extends('layouts.reset')
@section('content')
    <div class="container-fluid">
        <div class="image-holder2 d-lg-none d-xl-none">

        </div>

        <div class="login">
            <div align="center" class="logo" style="margin-bottom: 30px;">
                <a href="{{url('/')}}">
                    <img src="{{url('images/logo.png')}}" alt="recruit Logo">
                </a>
            </div>
            <div class="form-holder" align="center">
                @include('notification')

                @if(isset($change) && $change == true)

                <form method="post" action="{{url('reset-password')}}" autocomplete="off">
                    {{csrf_field()}}

                    <input type="hidden" name="email" value="{{$email}}">
                    <input type="hidden" name="role" value="{{$role}}">

                    <div class="email tags" name="frmCheckPassword" id="frmCheckPassword">
                        <i class="fas fa-lock icon"></i>
                        <input id="password" class="input" placeholder="New Password" name="newPassword" type="password" onKeyUp="checkPasswordStrength();" autocomplete="newPassword">
                        <hr>
                        <div id="password-strength-status"></div>

                    </div>
                    <div class="password tags">
                        <i class="fas fa-lock icon"></i>
                        <input class="input" placeholder="Confirm new password" name="confirmPassword" type="password" autocomplete="newPassword">
                        <hr>
                    </div>

                    <button type="submit" class="btn">Reset</button>
                </form>

                @endif
            </div>
        </div>
        <div class="image-holder">

        </div>
    </div>
@endsection