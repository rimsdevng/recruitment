@extends('layouts.company')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        {{--  <div class="col-md-8">
            <div class="card">
                <div class="card-header"><h2>Create Job Advert</h2></div>

                <div class="card-body">
                   <table>
                       
                   </table>
                </div>
            </div>
        </div>  --}}
        @include('notification')
        <div class="col-12 col-sm-12 col-lg-12 col-xl-12">
                <div class="card ">
                    <div class="card-header">
                        <div class="row">
                                <div class="col-md-9">
                                        <h4>Job Adverts</h4>
                                </div>
        
                                <div class="col-md-3">
                                        <a href="{{ url('/company/add-job-advert') }}" class="btn btn-success"><i class="fa fa-plus" style="margin"></i> Add New</a>
                                </div>
                                
                        </div>
                    </div>
                    <div class="card-body p-0 disco-bottom2">
                        <div class="table-responsive">
                            <table class="table mb-0 text-nowrap table-hover table-striped">
                                <tbody>
                                <tr>
                                    <th>S/N</th>
                                    <th>Position Title</th>
                                    <th>Job Category</th>
                                    <th>Application Reference No</th>
                                    <th>Recruit Company</th>
                                    <th>Position Date</th>
                                    <th>Closing Date</th>
                                    <th>Actions</th>
                                </tr>
                                @if(count($jobs)>0)

                                    <?php $count = 1; ?>
                    
                                @foreach($jobs as $j)
                                <tr>
                                    <td><?php echo $count;?></td>
                                    <td>{{ $j->positionTitle }}</td>
                                    <td>{{ $j->Jobcategory->name }}</td>
                                    <td> {{ $j->jobRefNo }}   </td>
                                    <td>{{ $j->Company['companyName']}}</td>
                                    <td>{{ $j->positionDate	 }}</td>
                                    <td>{{ $j->closingDate }}</td>
                                    <td>
                                        <a href="" class=" btn btn-primary">Edit</a>
                                        <a href="{{url('/company/'.$j->jpaid.'/delete-job-advert')}}" class=" btn btn-danger">Delete</a>
                                       
                                    </td>
                                    

                                </tr>
                                <?php $count ++; ?>
                                @endforeach
                                @else

                                    <tr>
                                        <td colspan="4" style="color: silver; text-align: center; margin-top: 30px;"> No Job Advert Created </td>
                                    </tr>
                                @endif
                            </tbody></table>
                        </div>
                    </div>
                </div>
            </div>
    </div>
</div>
@endsection
