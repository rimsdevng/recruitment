@extends('layouts.company')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"><h2>Create Job Advert</h2></div>
                @include('notification')
                <div class="card-body">
                    <form method="POST" action="{{ url('/company/post-job-advert') }}">
                        {{ csrf_field() }}

                        <input type="hidden" name="role" value="admin">

                        <div class="form-group row">
                            <label for="Job Category" class="col-md-4 col-form-label text-md-right">Job Category</label>
                            <div class="col-md-6">
                                    
                                <select class="form-control" name="jcid" id="jcid">
                                    @foreach($categories as $c)
                                    <option value="{{$c->jcid}}">{{$c->name}}</option>
                                    @endforeach
                                </select>    
                                @error('jcid')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="empid" class="col-md-4 col-form-label text-md-right">Recruit Company</label>
                            <div class="col-md-6">
                                <select class="form-control" name="empno" id="empno">
                                    @foreach( $companies as $company)
                                    <option value="{{$company->empno}}">{{$company->companyName}}</option>
                                    @endforeach
                                </select>  
                                @error('empid')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                        </div>
    
                        <div class="form-group row">
                            <label for="positionTitle" class="col-md-4 col-form-label text-md-right">Position Title</label>
                            <div class="col-md-6">
                                <input id="positionTitle" type="text" class="form-control @error('positionTitle') is-invalid @enderror" name="positionTitle" value="{{ old('positionTitle') }}" required autocomplete="positionTitle" autofocus>

                                @error('positionTitle')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="positionDescription" class="col-md-4 col-form-label text-md-right">Position Description</label>
                            <div class="col-md-6">
                                <input id="positionDescription" type="text" class="form-control @error('positionDescription') is-invalid @enderror" name="positionDescription" value="{{ old('positionDescription') }}" required autocomplete="positionDescription" autofocus>

                                @error('positionDescription')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                            
                        <div class="form-group row">
                            <label for="positionDate" class="col-md-4 col-form-label text-md-right">Position Date</label>
                            <div class="col-md-6">
                                <input id="positionDate" type="date" class="form-control @error('positionDate') is-invalid @enderror" name="positionDate" max="{{ \Carbon\Carbon::tomorrow()->format('Y-M-D') }}"  value="{{ old('positionDate') }}" required autocomplete="positionDate" autofocus>

                                @error('positionDate')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                          
                        <div class="form-group row">
                            <label for="closingDate" class="col-md-4 col-form-label text-md-right">Closing Date</label>
                            <div class="col-md-6">
                                <input id="closingDate" type="date" class="form-control @error('closingDate') is-invalid @enderror" name="closingDate" value="{{ old('closingDate') }}" required autocomplete="closingDate"  max="{{ \Carbon\Carbon::tomorrow()->format('Y-M-D') }}" autofocus>

                                @error('closingDate')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="closingTime" class="col-md-4 col-form-label text-md-right">Closing Time</label>
                            <div class="col-md-6">
                                <input id="closingTime" type="time" class="form-control @error('closingTime') is-invalid @enderror" name="closingTime" value="{{ old('closingTime') }}" required autocomplete="closingTime" autofocus>

                                @error('closingTime')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="jobDetails" class="col-md-4 col-form-label text-md-right">Job Details</label>
                            <div class="col-md-6">
                                <textarea id="jobDetails" type="text" class="form-control @error('jobDetails') is-invalid @enderror" name="jobDetails" value="{{ old('jobDetails') }}" cols="30" rows="3"></textarea>

                                @error('jobDetails')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="jobInstruction" class="col-md-4 col-form-label text-md-right">Job Instruction</label>
                            <div class="col-md-6">
                                <textarea id="jobInstruction" type="text" class="form-control @error('jobInstruction') is-invalid @enderror" name="jobInstruction" value="{{ old('jobInstruction') }}" cols="30" rows="3"></textarea>

                                @error('jobInstruction')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                      

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    Create
                                </button>

                                <a href="{{ url('/company/dashboard') }}" class="btn btn-warning pull-right">Cancel</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
