@extends('layouts.company')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        {{--  <div class="col-md-8">
            <div class="card">
                <div class="card-header"><h2>Create Job Advert</h2></div>

                <div class="card-body">
                   <table>
                       
                   </table>
                </div>
            </div>
        </div>  --}}
        @include('notification')
        <div class="col-12 col-sm-12 col-lg-12 col-xl-12">
                <div class="card ">
                    <div class="card-header">
                        <h4>List of Applicants</h4>
                    </div>
                    <div class="card-body p-0 disco-bottom2">
                        <div class="table-responsive">
                            <table class="table mb-0 text-nowrap table-hover table-striped">
                                <tbody>
                                <tr>
                                    <th>S/N</th>
                                    <th>First Name</th>
                                    <th>Last Name</th>
                                    <th>Reference No</th>
                                    <th>Status</th>
                                    <th>CV</th>
                                    <th>Created At</th>
                                    <th>Actions</th>
                                </tr>
                                @if(count($applications)>0)

                                    <?php $count = 1; ?>
                    
                                @foreach($applications as $application)
                                <tr>  
                                    <td><?php echo $count;?></td>
                                    <td>{{ $application->fname }}</td>
                                    <td>{{ $application->sname }}</td>
                                    <td> {{ $application->studentNo }}   </td>
                                    <td class="success">{{ $application->status }}</td>
                                    <td><a href="{{ $application->filecv}}">Download Attached File</a></td>
                                    <td>{{ $application->created_at	 }}</td>
                                  
                                    <td>
                                        <a href="{{ url('/company/delete/'.$application->apid.'/application') }}" class=" btn btn-danger">Delete</a>
                                @if($application->status != 'Approved')    
                                    <a href="{{ url('/company/approve-application/'.$application->apid) }}" class="btn btn-success">Approve</a>
                                    @elseif($application->status == 'Approved' || $application->status != 'UnQualified')    

                                    <a href="{{ url('/company/decline-application/'.$application->apid) }}" class="btn btn-warning">Decline</a>

                                @endif    
                                    </td>
                                    

                                </tr>
                                <?php $count ++; ?>
                                @endforeach
                                @else

                                    <tr>
                                        <td colspan="4" style="color: silver; text-align: center; margin-top: 30px;"> No Job Advert Created </td>
                                    </tr>
                                @endif
                            </tbody></table>
                        </div>
                    </div>
                    {{ $applications->links() }}
                </div>
            </div>
    </div>
</div>
@endsection
