@extends('layouts.company')

@section('content')

<div class="container">

    <div class="col-md-12 mb-12"  style="margin-bottom: 25px;">
        <h2 align="left">
            <strong>
                Welcome {{session()->get('company')->companyName}}
            </strong>
        </h2>
  </div>
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Company Dashboard</div>
                <div class="container" style="padding:30px;">
                    <div class="row">
                       <div class="col-md-4">
                         <div class="card" align="center">
                           <h4> Pending Applications</h4>
                           <span>{{ count($pendingApplications) }}</span>
                         </div>
                       </div>

                       <div class="col-md-4">
                          <div class="card" align="center">
                            <h4>Job Adverts Published</h4>
                            <span>{{ count($advertsPublished) }}</span>
                          </div>
                        </div>

                        <div class="col-md-4">
                            <div class="card" align="center">
                              <h4>Successfull Applicants</h4>
                              <span>{{ count($approvedApplications) }}</span>
                            </div>
                          </div>
                    </div>
                </div>
              </div>
        </div>
    </div>
    <div class="row" align="center" style="padding: 80px">
      <div class="col-md-3">
        <a href="{{ url('/company/add-job-advert') }}" class="btn btn-primary" style="color:#ffffff;">
          Create Job advert
        </a>
      </div>

      <div class="col-md-3">
        <a href="{{ url('/company/view-job-adverts') }}" class="btn btn-warning" style="color:#ffffff;">
          View Job Adverts
        </a>
      </div>   

      <div class="col-md-3">
        <a href="{{ url('/company/view-applications') }}" class="btn btn-secondary">
          View Applicants
        </a>
      </div>
      
      <div class="col-md-3">
        <a href="{{ url('/company/category/add') }}" class="btn btn-success">
          Create Job Category
        </a>
      </div>
    </div>
   
</div>
@endsection
















{{--  

<ul>
        <li><a style="color:black !important;" href="{{url('change-password')}}"><i class="ti-lock"></i> <span>Change Password</span></a></li>
        @if(session()->has('staff'))
            @php(  $user = session()->get('staff'))
            <li><a style="color:black !important;" href="{{url('logout-user/'.$user->role)}}"><i class="ti-power-off"></i> <span>Logout</span></a></li>

        @endif
    </ul>  --}}