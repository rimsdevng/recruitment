<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Recruit</title>
    <link rel="stylesheet" href="{{ url('assets/bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ url('assets/fonts/font-awesome.min.css') }}">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Nunito">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Nunito+Sans">
    <link rel="stylesheet" href="{{ url('assets/css/Sidebar-Menu.css') }}">
    <link rel="stylesheet" href="{{ url('assets/css/Sidebar-Menu1.css') }}">
    <link rel="stylesheet" href="{{ url('assets/css/styles.css') }}">
</head>

<body>
    <div class="row cont" style="/*min-height:100%;*//*height:100vh;*/position:relative;">
            @yield('content')
    </div>
    <script src="{{ url('assets/js/jquery.min.js') }}"></script>
    <script src="{{ url('assets/bootstrap/js/bootstrap.min.js') }}"></script>
    <script src="{{ url('assets/js/Chart.min.js') }}"></script>
    <script src="{{ url('assets/js/script.js') }}"></script>
    <script src="{{ url('assets/js/script.js') }}"></script>
</body>

</html>