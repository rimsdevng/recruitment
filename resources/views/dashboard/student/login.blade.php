@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
            @include('notification')
                <div class="card-header">{{ __('Student Login') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ url('student/login')}}">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Submit') }}
                                </button>

                                @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ url('/student/password/reset') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                @endif


                                    <a class="btn btn-link" href="{{ url('/student/register') }}">
                                        {{ __('Register here?') }}
                                    </a>

                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection







{{--  @extends('dashboard.student.layouts.auth')

@section('content')


    <div class="col-md-8 d-none d-md-block">
        <div class="side-1"></div>
        <div class="side-1-overlay"></div> 
        <div class="info-side">
                <div class="main-info">
                
                <h1>Wecome to Student Portal</h1>
                <h6> It is our pleasure to provide you with the best Job Offer. </br></h6>
                <a href="{{ url('/') }}">
                <button class="btn btn-primary ob-btn btn-light" type="button">Learn more</button><button class="btn btn-primary ob-btn btn-flat" type="button" >Go Back</button>
                </a>
            </div> 
            <!-- the span class below has a Home link caption -->  
        </div>
    </div>
    <div class="col">
        <div class="login-container">
            @include('notification')
             <a href="{{ url('/') }}">
                <img src="{{ url('images/logo.png') }}" alt="Recruitment">
            </a>
            <h6 class="sub-title"><br>Please login to proceed</h6> 
            <form id="login-form" role="form" method="POST" action="{{ url('student/login')}}">

                {{ csrf_field() }}

                @if ($errors->has('email'))
                    <div class="error" align="center">
                        <strong>{{ $errors->first('email') }}</strong>
                    </div>
                @endif

          
                <div class="form-group input-field s12 {{ $errors->has('email') ? ' has-error' : '' }}"><label>Email<br></label>
                    <input class="form-control validate" id="email" type="text" name="email" value="{{ old('email') }}" required autofocus>
                </div>

                @if ($errors->has('password'))
                <span class="error">
                            <strong>{{ $errors->first('password') }}</strong>
                </span>
                @endif

                <div class="form-group input-field s12  {{ $errors->has('password') ? ' has-error' : '' }}">
                    <label for="password">Password</label>
                    <input class="form-control" id="password" type="password" name="password" required>
                </div>
                <div class="col-md-6 col-md-offset-4">
                    <input type="checkbox" id="remember" name="remember" />
                    <label for="remember">Remember Me</label>
                </div>
                <p id="fgt-pass"><a href="{{ url('/student/password/reset') }}">Forgot Password?</a></p>
                

                <div class="btn-cont"><button class="btn btn-primary ob-btn btn-dark" type="submit">Login </button></div>
            </form>
            <div class="bottom">
                <p>You dont have account? Kindly Click to  <span> <a href="{{ url('/student/register') }}" style="color: red !important; "> <strong>Register</strong></a></span><br></p>
            </div>

            
        </div>
    </div>
@endsection


  --}}
