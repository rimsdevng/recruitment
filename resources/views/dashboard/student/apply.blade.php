@extends('layouts.student')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                <h2>Apply for Job</h2>
                <small>Kindly Fill in the details below to complete your Application</small>
                </div>
            
                @include('notification')
                <div class="card-body">
                        {{--  @if( (count($assignment->Submission) > 0 ) && ($assignment->Submission->sid == session()->get('student')->sid) )  --}}
                            <form method="POST" action="{{ url('/student/post-application/') }}"  enctype="multipart/form-data">
                                {{ csrf_field() }}

                                <div class="form-group row">
                                    <label for="sname" class="col-md-4 col-form-label text-md-right">Full Name</label>
                                    <div class="col-md-3">
                                        <input id="sname" type="text" class="form-control @error('sname') is-invalid @enderror" name="sname" value="{{ session()->get('student')->sname }}" required autocomplete="sname" autofocus>

                                        @error('sname')
                                        <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>

                                    <div class="col-md-3">
                                        <input id="fname" type="text" class="form-control @error('fname') is-invalid @enderror" name="fname" value="{{ session()->get('student')->fname }}" required autocomplete="fname" autofocus>

                                        @error('sname')
                                        <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="Job Category" class="col-md-4 col-form-label text-md-right">Select Job Category</label>
                                    <div class="col-md-6">
                                            
                                        <select class="form-control" name="jcid">
                                            @foreach($categories as $c)
                                            <option value="{{$c->jcid}}">{{$c->name}}</option>
                                            @endforeach
                                        </select>    
                                        @error('jcid')
                                        <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                
                                <div class="form-group row">
                                        <label for="Job Title" class="col-md-4 col-form-label text-md-right">Select Job Title</label>
                                        <div class="col-md-6">
                                                
                                            <select class="form-control" name="jpaid">
                                                @foreach($jobadverts as $ja)
                                                <option value="{{$ja->jpaid}}">{{$ja->positionTitle}}</option>
                                                @endforeach
                                            </select>    
                                            @error('jcid')
                                            <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                
                                <div class="form-group row">
                                    <label for="comment" class="col-md-4 col-form-label text-md-right">Give Application Details</label>
                                    <div class="col-md-6">
                                        <textarea id="comment" type="text" class="form-control @error('comment') is-invalid @enderror" name="comment" value="{{ old('comment') }}" cols="30" rows="3"></textarea>

                                        @error('comment')
                                        <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="document" class="col-md-4 col-form-label text-md-right">Upload CV</label>
                                    <div class="col-md-6">
                                        <input type="file" type="text" class="form-control @error('document') is-invalid @enderror" name="document" required>

                                        @error('document')
                                        <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                            

                                <div class="form-group row mb-0">
                                    <div class="col-md-8 offset-md-4">
                                        <button type="submit" class="btn btn-primary">
                                        Apply
                                        </button>

                                        <a href="{{ url('/student/dashboard') }}" class="btn btn-warning pull-right">Cancel</a>
                                    </div>
                                </div>
                            </form>

                        {{--  @else
                        @endif  --}}
                </div>
            </div>
        </div>
    </div>
</div>



















   {{--  <input type="hidden" name="role" value="admin">  --}}

                   

                        {{--  <div class="form-group row">
                            <label for="empid" class="col-md-4 col-form-label text-md-right">Recruit Company</label>
                            <div class="col-md-6">
                                <select class="form-control" name="empno" id="empno">
                                    @foreach( $companies as $company)
                                    <option value="{{$company->empno}}">{{$company->companyName}}</option>
                                    @endforeach
                                </select>  
                                @error('empid')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                        </div>  --}}
    
                        {{--  <div class="form-group row">
                            <label for="positionTitle" class="col-md-4 col-form-label text-md-right">Position Title</label>
                            <div class="col-md-6">
                                <input id="positionTitle" type="text" class="form-control @error('positionTitle') is-invalid @enderror" name="positionTitle" value="{{ old('positionTitle') }}" required autocomplete="positionTitle" autofocus>

                                @error('positionTitle')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="positionDescription" class="col-md-4 col-form-label text-md-right">Fill In Application Details</label>
                            <div class="col-md-6">
                                <input id="positionDescription" type="text" class="form-control @error('positionDescription') is-invalid @enderror" name="positionDescription" value="{{ old('positionDescription') }}" required autocomplete="positionDescription" autofocus>

                                @error('positionDescription')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                            
                        <div class="form-group row">
                            <label for="positionDate" class="col-md-4 col-form-label text-md-right">Position Date</label>
                            <div class="col-md-6">
                                <input id="positionDate" type="date" class="form-control @error('positionDate') is-invalid @enderror" name="positionDate" value="{{ old('positionDate') }}" required autocomplete="positionDate" autofocus>

                                @error('positionDate')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                          
                        <div class="form-group row">
                            <label for="closingDate" class="col-md-4 col-form-label text-md-right">Closing Date</label>
                            <div class="col-md-6">
                                <input id="closingDate" type="date" class="form-control @error('closingDate') is-invalid @enderror" name="closingDate" value="{{ old('closingDate') }}" required autocomplete="closingDate" autofocus>

                                @error('closingDate')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="closingTime" class="col-md-4 col-form-label text-md-right">Closing Time</label>
                            <div class="col-md-6">
                                <input id="closingTime" type="time" class="form-control @error('closingTime') is-invalid @enderror" name="closingTime" value="{{ old('closingTime') }}" required autocomplete="closingTime" autofocus>

                                @error('closingTime')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>  --}}

@endsection
