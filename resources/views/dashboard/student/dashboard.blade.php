@extends('layouts.student')

@section('content')
<div class="container">
        <div class="row">
          <div class="col-md-12 mb-12"  style="margin-bottom: 25px;">
                <h2 align="left">
                    <strong>
                        Welcome {{session()->get('student')->fname}}
                    </strong>
                </h2>
          </div>
        </div>
    <div class="row justify-content-center">
        <div class="col-md-8">
            @include('notification')
            
            <div class="card">
                <div class="card-header">Student Dashboard</div>
                <div class="container">
                   
                    <div class="row">
                            <div class="col-md-8">
                                    <h2> Recent Activities</h2>
                                    <hr>
                                    <p>Your Application has been submitted</p>
                                    <p>You just changed your password 3mins ago</p>
                            </div>
                           
                            <div class="col-md-3">
                                <div class="card">
                                    <a href="{{ url('change-password') }}" align="center">Change Password</a>
                                </div>
                            </div>  
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row" align="center" style="padding: 80px">
        <div class="col-md-4 offset-2" align="center">
            <a href="{{ url('/student/apply') }}" class="btn btn-primary">
                Apply for a Job
            </a>
        </div>
    
       <div class="col-md-4 col-offset-2">
            <a href="" class="btn btn-secondary">
                View Notifications
            </a>
        </div>   

        
    </div>
</div>
@endsection
















{{--  

<ul>
        <li><a style="color:black !important;" href="{{url('change-password')}}"><i class="ti-lock"></i> <span>Change Password</span></a></li>
        @if(session()->has('staff'))
            @php(  $user = session()->get('staff'))
            <li><a style="color:black !important;" href="{{url('logout-user/'.$user->role)}}"><i class="ti-power-off"></i> <span>Logout</span></a></li>

        @endif
    </ul>  --}}