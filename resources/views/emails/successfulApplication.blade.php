@component('mail::message')
# Welcome to Recruit

Hello {{ $application->fname }},

Your Application: <strong>{{ $application->applicationNo }}</strong> is <strong>{{ $application->status }}</strong>

Your Student Reference Number: <strong>{{ $application->studentNo }}</strong>

@component('mail::button', ['url' => 'localhost:8000/'])
Proceed
@endcomponent

Thanks,<br>
Recruit
@endcomponent
