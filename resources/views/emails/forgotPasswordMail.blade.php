<style>
        .btn-primary {
            color: #fff;
            background-color: #dc3434;
            border-color: #dc3434;
            width: 150px;
        }
        .btn {
            display: inline-block;
            font-weight: 400;
            text-align: center;
            white-space: nowrap;
            vertical-align: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            border: 1px solid transparent;
            padding: .375rem .75rem;
            font-size: .9rem;
            line-height: 1.6;
            border-radius: .25rem;
            transition: color .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,box-shadow .15s ease-in-out;
        }
</style>
@component('mail::message')
# Please click the link below to reset your password. <br>


<a class="btn btn-primary" href="{{'http://localhost:8000/reset-password?token=' . $token . '&email=' . $email. '&role=' . $role}}">here</a>

Thanks,<br>
Recruit
@endcomponent
