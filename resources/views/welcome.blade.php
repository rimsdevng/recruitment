@extends('layouts.app')

@section('content')
    <style>
        html, body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Nunito', sans-serif;
            font-weight: 200;
            height: 100vh;
            margin: 0;
        }

        .full-height {
            height: 100vh;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }

        .content {
            text-align: center;
        }

        .title {
            font-size: 84px;
        }

        .m-b-md {
            margin-bottom: 30px;
        }
    </style>
    <div class="container">
            <div class="col-md-12 mb-12">

                    <div class="jumbotron">
                        <h1 class="display-4">Welcome to </h1>
                    <h3 class="lead">Web Based Application for Secure College Student Recruitment in Nigeria .</h3>
                    <hr class="my-4">
                    </div>
              </div>
     
        @if(session()->has('student'))
            <a href="{{url('student/dashboard')}}" align="center" class="btn btn-success">Go to Student Dashhoard</a>
            @elseif(session()->has('company'))
            <a href="{{ url('company/dashboard')}}" align="center" class="btn btn-success">Go to Company Dashhoard</a>
            @else
            <div class="row justify-content-center">
                    {{--  <div class="col-md-4">
                            <div class="card" style="width: 18rem;">
                                <div class="card-body">
                                    <a href="{{url('/login')}}">
                                        <h5 class="card-title">Admin Login</h5>
                                    </a>
        
                                </div>
                            </div>
                        </div>  --}}
                <div class="off-set-4 col-md-4">
                    <div class="card" style="width: 18rem;">
                        <div class="card-body">
                            <a href="{{url('student/login')}}">
                                <h5 class="card-title">Student Login</h5>
                            </a>

                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card" style="width: 18rem;">
                        <div class="card-body">
                            <a href="{{url('company/login')}}">
                                <h5 class="card-title">Recruitment Company Login </h5>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        @endif

    </div>
@endsection

