<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class JobdvertApplication extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create( 'jobdvert_application', function ( Blueprint $table ) {
            $table->increments( 'jaid' );
            $table->integer('jpaid');
            $table->integer('apid');
			$table->softDeletes();
			$table->timestamps();

        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        
        Schema::dropIfExists('jobdvert_application');
        
    }
}
