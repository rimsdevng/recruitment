<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Company extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        
        Schema::create( 'companies', function ( Blueprint $table ) {
			$table->increments( 'empno' );
			$table->string('companyid')->nullable();
            $table->integer('uid')->nullable();
            $table->string('phone')->nullable();
            $table->string('companyName')->nullable();
            $table->string('companyWebsite')->nullable();
            $table->string('companyAddress', 2000)->nullable();
            $table->string('email')->unique()->nullable();
			$table->string('password');
			$table->softDeletes();
			$table->timestamps();

		} );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
