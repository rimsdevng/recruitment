<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InitialDB extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('job_notifications', function (Blueprint $table) {
            $table->increments('nid');
            $table->string('studentNo');
            $table->string('fname');
            $table->string('sname');
            $table->string('jobRefNo');
            $table->string('jobAdvertNo');
            $table->string('message', 2000);
            $table->timestamps();
        });

        Schema::create('password_resets', function (Blueprint $table) {
            $table->string('email')->index();
            $table->string('token');
            $table->timestamp('created_at')->nullable();
        });


        
        Schema::create('applications', function (Blueprint $table){
		    $table->increments('apid');
            $table->integer('jcid')->nullable();
            $table->integer('japid')->nullable();
            $table->string('studentNo')->nullable();
		    $table->string('sname')->nullable();
		    $table->string('fname')->nullable();
		    $table->string('oname')->nullable();
		    $table->string('dob')->nullable();
		    $table->enum('gender',['male','female'])->nullable();
		    $table->string('address')->nullable();
			$table->string('phone')->nullable();
            $table->string('email')->index()->nullable();
            $table->string('url')->nullable(); //This is coverdetails
			$table->string('comment')->nullable();
            $table->string('religion_denomination')->nullable();
            $table->string('nationality')->nullable();
            $table->string('state')->nullable();
            $table->enum('status',['Awaiting','Processing','Qualified','Approved','Endorsed','UnQualified'])->nullable();
    

		    $table->timestamps();
		    $table->softDeletes();

        });

        Schema::create('job_position_adverts', function (Blueprint $table){
		    $table->increments('jpaid');
            $table->integer('jcid');
            $table->integer('jobRefNo');
            $table->string('empid');
		    $table->string('positionTitle')->nullable();
		    $table->string('positionDescription')->nullable();
		    $table->string('positionDate')->nullable();
		    $table->string('closingDate')->nullable();
		    $table->string('closingTime')->nullable();
			$table->string('jobDetails', 2000)->nullable();
            $table->string('jobInstruction')->index()->nullable();
            $table->enum('status',['isAvailable','isUnAvailable','isCanceled'])->default('isAvailable')->nullable();
		    $table->timestamps();
		    $table->softDeletes();

        });

        Schema::create('images', function (Blueprint $table) {
            $table->increments('imid');
            $table->string('url',2000);
            $table->integer('apid')->nullable();
            $table->integer('uid')->nullable();
            $table->timestamps();
        });

        Schema::create('jobcategories', function (Blueprint $table) {
            $table->increments('jcid');
            $table->string('name');
            $table->string('description')->nullable();
            $table->timestamps();
        });



    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
        Schema::dropIfExists('password_resets');
    }
}
