<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Student extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create( 'students', function ( Blueprint $table ) {
			$table->increments( 'stid' );
			$table->string('studentno')->nullable();
			// $table->integer('uid')->nullable();
			// $table->string('title')->nullable();
            $table->string('fname')->nullable();
            $table->string('sname')->nullable();
            $table->string('username')->nullable();
            $table->enum( 'gender', [ 'male', 'female' ])->default('male');
			$table->string('email')->nullable();
			$table->string( 'address', 2000 )->nullable();
			$table->string('phone')->nullable();
            $table->string('password')->nullable();
            $table->enum( 'status', [ 'male', 'female' ])->default('male');

			$table->softDeletes();
			$table->timestamps();

		} );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
